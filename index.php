<?php
require_once("init.php");
require_once("helpers.php");
require_once("data.php");
require_once("my_functions.php");
require_once("models.php");

if (!$link) {
    print('<p style = "color: red;">Нет подключения</p>'); // Сигнализация подключения
    $error = mysqli_connect_error();
    $content = include_template('error.php', ['error' => $error]);
} else {
    print('<p style = "color: green;
    padding-left: 15px;">Успешное подключение</p>'); // Сигнализация подключения
    // $sql = get_categories($link);
    $sql = "SELECT character_code, name_category FROM categories";
    $result = mysqli_query($link, $sql);
    if ($result) {
        $categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
    } else {
        $error = mysqli_error($link);
    }
}

$sql = get_query_list_lots('2023-11-6');

$res = mysqli_query($link, $sql);
if ($res) {
    $goods = mysqli_fetch_all($res, MYSQLI_ASSOC);
} else {
    $error = mysqli_error($link);
    $content = include_template('error.php', ['error' => $error]);
}

$page_content = include_template("../templates/main.php", [
    "categories" => $categories,
    "goods" => $goods
]);
$layout_content = include_template("../templates/layout.php", [
    "content" => $page_content,
    "categories" => $categories,
    "title" => "Главная"
]);

print($layout_content);