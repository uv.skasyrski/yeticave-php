<?php
require_once("init.php");
require_once("helpers.php");
require_once("data.php");
require_once("my_functions.php");
require_once("models.php");

$categories = get_categories($link);

$page_404 = include_template("404.php", [
    "categories" => $categories
]);

// if (!$link) {
//     $error = mysqli_connect_error();
//  } else {
//     $sql = "SELECT character_code, name_category FROM categories";
//     $result = mysqli_query($link, $sql);
//     if ($result) {
//          $categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
//      } else {
//          $error = mysqli_error($con);
//        }
//  }

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

if ($id) {
    $sql = get_query_lot ($id);
} else {
    // print($page_404);
    http_response_code(404);
    die();
};

$res = mysqli_query($link, $sql);
if ($res) {
    // $lot = mysqli_fetch_assoc($res);
    $lot = get_arrow($res);
} else {
    $error = mysqli_error($link);
};

if(!$lot) {
    http_response_code(404);
    die();
}

$page_content = include_template("main_lot.php", [
    "categories" => $categories,
    "lot" => $lot
]);
$layout_content = include_template("layout_lot.php", [
    "content" => $page_content,
    "categories" => $categories,
    "title" => $lot["title"],
    "is_auth" => $is_auth,
    "user_name" => $user_name
]);

print($layout_content);