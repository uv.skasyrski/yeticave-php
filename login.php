<?php
require_once("helpers.php");
require_once("my_functions.php");
require_once("data.php");
require_once("init.php");
require_once("models.php");

$is_auth = 0;
$user_name = "";
$user_info = [];
$categories = get_categories($link);

$page_content = include_template("main_login.php", [
    "categories" => $categories
]);


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $required = ["email", "password"];
    $errors = [];

    $rules = [
        "email" => function ($value) {
            return validate_email($value);
        },
        "password" => function ($value) {
            return validate_length($value, 6, 8);
        }
    ];

    $user_info = filter_input_array(INPUT_POST,
        [
            "email" => FILTER_DEFAULT,
            "password" => FILTER_DEFAULT
        ], true);

        if ($user_info) {
            print('<p style = "color: green;">$user_info!</p>');
        }
        print('<p style = "color: red;">Нет переменной $user_info</p>');

    foreach ($user_info as $field => $value) {
        if (isset($rules[$field])) {
            $rule = $rules[$field];
            $errors[$field] = $rule($value);
        }
        if (in_array($field, $required) && empty($value)) {
            $errors[$field] = "Поле $field нужно заполнить";
        }
    }

    $errors = array_filter($errors);


    if (count($errors)) {
        $page_content = include_template("main_login.php", [
            "categories" => $categories,
            "user_info" => $user_info,
            "errors" => $errors
        ]);
    } else {
        $users_data = get_login($link, $user_info["email"]);
        if ($users_data) {
            if (password_verify($user_info["password"], $users_data["user_password"])) {
                $issession = session_start();
                $_SESSION['name'] = $users_data["user_name"];
                $_SESSION['id'] = $users_data["id"];
                $user_name = $users_data["user_name"];

                header("Location: /index.php");
            } else {
                $errors["password"] = "Вы ввели неверный пароль";
            }
        } else {
            $errors["email"] = "Пользователь с таким е-mail не зарегестрирован";
        }
        if (count($errors)) {
            $page_content = include_template("main-login.php", [
                "categories" => $categories,
                "user_info" => $user_info,
                "errors" => $errors
            ]);
        }
    }
}


$layout_content = include_template("layout_login.php", [
    "content" => $page_content,
    "categories" => $categories,
    "title" => "Регистрация",
    "is_auth" => $is_auth,
    "user_name" => $user_name
]);



print($layout_content);


